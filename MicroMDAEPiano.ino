/*
   MicroMDAEPiano

   MicroMDAEPiano is a port of the MDA-EPiano sound engine
   (https://sourceforge.net/projects/mda-vst/) for the Teensy-3.5/3.6 with audio shield.

   (c)2019-2020 H. Wirtz <wirtz@parasitstudio.de>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include <MIDI.h>
#include <EEPROM.h>
#include "EEPROMAnything.h"
#include "mdaEPiano.h"
#include "source_micromdaepiano.h"
#include "effect_modulated_delay.h"
#ifdef PATCHED_FREEVERB
#include "my_effect_freeverb.h"
#endif
#ifdef USE_XFADE_DATA
#include "mdaEPianoDataXfade.h"
#else
#include "mdaEPianoData.h"
#endif
#include "UI.hpp"
#include "midi_devices.hpp"
#include "config.h"

//*************************************************************************************************
//* GLOBAL VARIABLES
//*************************************************************************************************

// Audio configuration
AudioSourceMicroMDAEPiano ep;
AudioAnalyzePeak         peak_r;
AudioAnalyzePeak         peak_l;
#ifndef PATCHED_FREEVERB
AudioEffectFreeverb        freeverb_r;
AudioEffectFreeverb        freeverb_l;
#else
MyAudioEffectFreeverb        freeverb_r;
MyAudioEffectFreeverb        freeverb_l;
#endif
AudioMixer4              mixer_r;
AudioMixer4              mixer_l;
AudioAmplifier           volume_r;
AudioAmplifier           volume_l;
AudioAmplifier           inverter;
AudioEffectModulatedDelay   modchorus_r;
AudioEffectModulatedDelay   modchorus_l;
#if MOD_FILTER_OUTPUT != MOD_NO_FILTER_OUTPUT
AudioFilterBiquad        modchorus_filter_r;
AudioFilterBiquad        modchorus_filter_l;
#endif
AudioSynthWaveform       modulator;
AudioConnection          patchCord0(ep, 0, peak_r, 0);
AudioConnection          patchCord1(ep, 1, peak_l, 0);
AudioConnection          patchCord2(ep, 0, freeverb_r, 0);
AudioConnection          patchCord3(ep, 1, freeverb_l, 0);
AudioConnection          patchCord4(ep, 0, modchorus_r, 0);
AudioConnection          patchCord5(ep, 1, modchorus_l, 0);
AudioConnection          patchCord6(modulator, 0, modchorus_r, 1);
AudioConnection          patchCord7(modulator, inverter);
AudioConnection          patchCord8(inverter, 0, modchorus_l, 1);
AudioConnection          patchCord9(ep, 0, mixer_r, 0);
AudioConnection          patchCord10(ep, 1, mixer_l, 0);
#if MOD_FILTER_OUTPUT != MOD_NO_FILTER_OUTPUT
AudioConnection          patchCord11(modchorus_r, modchorus_filter_r);
AudioConnection          patchCord12(modchorus_l, modchorus_filter_l);
AudioConnection          patchCord13(modchorus_filter_r, 0, mixer_r, 2);
AudioConnection          patchCord14(modchorus_filter_l, 0, mixer_l, 2);
#else
AudioConnection          patchCord11(modchorus_r, mixer_r);
AudioConnection          patchCord12(modchorus_l, mixer_l);
#endif
AudioConnection          patchCord15(freeverb_r, 0, mixer_r, 1);
AudioConnection          patchCord16(freeverb_l, 0, mixer_l, 1);
AudioConnection          patchCord17(mixer_r, volume_r);
AudioConnection          patchCord18(mixer_l, volume_l);
#ifdef USB_AUDIO
AudioOutputUSB           usb1;
AudioConnection          patchCord19(volume_r, 0, usb1, 0);
AudioConnection          patchCord20(volume_l, 0, usb1, 1);
#endif
AudioOutputI2S           i2s1;
AudioConnection          patchCord21(volume_r, 0, i2s1, 0);
AudioConnection          patchCord22(volume_l, 0, i2s1, 1);
AudioControlSGTL5000     sgtl5000_1;

extern void init_menus(void);
extern int32_t encoder_value[NUM_ENCODER];
extern Bounce but[NUM_ENCODER];

// more variables
uint8_t sound = 1;
uint32_t xrun = 0;
uint32_t overload = 0;
uint32_t peak = 0;
uint16_t render_time_max = 0;
elapsedMicros fill_audio_buffer;
elapsedMillis control_rate;
const uint16_t audio_block_time_us = 1000000 / (SAMPLE_RATE / AUDIO_BLOCK_SAMPLES);
config_t configuration = {
  0xffff,                               // checksum
  ENC_DECAY_DEFAULT,                    // decay
  ENC_RELEASE_DEFAULT,                  // release
  ENC_HARDNESS_DEFAULT,                 // hardness
  ENC_TREBLE_DEFAULT,                   // treble
  ENC_STEREO_DEFAULT,                   // stereo
  ENC_TRANSPOSE_DEFAULT,                // transpose
  ENC_TUNE_DEFAULT,                     // tune
  ENC_DETUNE_DEFAULT,                   // detune
  ENC_VELOCITY_SENSE_DEFAULT,           // velocity_sense
  ENC_PAN_TREM_FREQUENCY_DEFAULT,       // pan_trem_frequency
  ENC_PAN_TREM_LEVEL_DEFAULT,           // pan_trem_level
  ENC_OVERDRIVE_DEFAULT,                // overdrive
  ENC_COMP_GAIN_DEFAULT,                // comp_gain
  ENC_COMP_RESPONSE_DEFAULT,            // comp_response
  ENC_COMP_LIMIT_DEFAULT,               // comp_limit
  ENC_COMP_THRESHOLD_DEFAULT,           // comp_threshold
  ENC_COMP_ATTACK_DEFAULT,              // comp_attack
  ENC_COMP_DECAY_DEFAULT,               // comp_decay
  ENC_REVERB_ROOMSIZE_DEFAULT,          // reverb_roomsize
  ENC_REVERB_DAMPING_DEFAULT,           // reverb_damping
  ENC_REVERB_LEVEL_DEFAULT,             // reverb_level
  ENC_CHORUS_FREQUENCY_DEFAULT,         // chorus_frequency
  ENC_CHORUS_INTENSITY_DEFAULT,         // chorus_intensity
  ENC_CHORUS_WAVEFORM_DEFAULT,          // chorus_waveform
  ENC_CHORUS_LEVEL_DEFAULT,             // chorus_level
  ENC_BASS_LR_LEVEL_DEFAULT,            // bass_lr_level
  ENC_BASS_MONO_LEVEL_DEFAULT,          // bass_mono_level
  ENC_EQ_BASS_DEFAULT,                  // eq_bass
  ENC_EQ_TREBLE_DEFAULT,                // eq_treble
  ENC_LOUDNESS_DEFAULT,                 // loudness
  ENC_MIDI_CHANNEL_DEFAULT,             // midi_channel
  ENC_MIDI_SOFT_THRU_DEFAULT,           // midi_soft_thru
  ENC_MAX_POLY_DEFAULT,                 // max_poly
  ENC_MONO_DEFAULT,                     // mono
  ENC_MASTER_PAN_DEFAULT                // pan
};

uint8_t master_volume = ENC_MASTER_VOLUME_DEFAULT;
int8_t pan = ENC_MASTER_PAN_DEFAULT;
uint8_t eeprom_config_update_flag = 0;
bool eeprom_master_volume_update_flag = false;
elapsedMillis eeprom_master_volume_update_timer;

#ifdef SHOW_CPU_LOAD_MSEC
elapsedMillis cpu_mem_millis;
#endif

#ifdef DEBUG_AUDIO
elapsedMillis debug_audio_timer;
#endif

// Allocate the delay lines for left and right channels
short l_delayline[MOD_DELAY_SAMPLE_BUFFER];
short r_delayline[MOD_DELAY_SAMPLE_BUFFER];

enum { VOL_MAIN, VOL_REVERB, VOL_CHORUS };
//*************************************************************************************************
//* SETUP FUNCTION
//*************************************************************************************************

void setup()
{
  Serial.begin(SERIAL_SPEED);

  pinMode(BUT_L_PIN, INPUT_PULLUP);
  pinMode(BUT_R_PIN, INPUT_PULLUP);

  init_menus();

  // Debug output
  Serial.println(F("MicroMDAEPiano based on https://sourceforge.net/projects/mda-vst"));
  Serial.println(F("(c)2019-2020 H. Wirtz <wirtz@parasitstudio.de>"));
  Serial.println(F("https://codeberg.org/dcoredump/MicroMDAEPiano"));
  Serial.print(F("Data in PROGMEM: "));
  Serial.print(sizeof(epianoDataXfade), DEC);
  Serial.println(F(" bytes"));
  Serial.println();
  Serial.println(F("<setup start>"));

  // set initial init configuration
  set_complete_configuration();

  setup_midi_devices();

  // start audio card
  AudioNoInterrupts();
  AudioMemory(AUDIO_MEM);

  sgtl5000_1.enable();
  sgtl5000_1.dacVolumeRamp();
  sgtl5000_1.dacVolume(1.0);
  sgtl5000_1.unmuteHeadphone();
  sgtl5000_1.volume(0.5, 0.5); // Headphone volume
  sgtl5000_1.unmuteLineout();
  sgtl5000_1.lineOutLevel(SGTL5000_LINEOUT_LEVEL);
  sgtl5000_1.audioPostProcessorEnable();
  sgtl5000_1.eqSelect(TONE_CONTROLS);
  sgtl5000_1.autoVolumeEnable();
  sgtl5000_1.enhanceBassEnable();
  Serial.println(F("Teensy-Audio-Board enabled."));

#if defined (SHOW_DEBUG) && defined (SHOW_CPU_LOAD_MSEC)
  // Initialize processor and memory measurements
  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();
#endif

  AudioInterrupts();

  Serial.print(F("AUDIO_BLOCK_SAMPLES="));
  Serial.print(AUDIO_BLOCK_SAMPLES);
  Serial.print(F(" (Time per block="));
  Serial.print(audio_block_time_us);
  Serial.println(F("us)"));

  if (!modchorus_r.begin(r_delayline, MOD_DELAY_SAMPLE_BUFFER)) {
    Serial.println(F("AudioEffectModulatedDelay - right channel begin failed"));
    while (1);
  }
  if (!modchorus_l.begin(l_delayline, MOD_DELAY_SAMPLE_BUFFER)) {
    Serial.println(F("AudioEffectModulatedDelay - left channel begin failed"));
    while (1);
  }
#ifdef DEBUG
  Serial.print(F("MOD_DELAY_SAMPLE_BUFFER="));
  Serial.print(MOD_DELAY_SAMPLE_BUFFER, DEC);
  Serial.println(F(" samples"));
#endif

  // chorus modulation fixed
  modulator.begin(MOD_WAVEFORM);
  modulator.phase(0);
  modulator.amplitude(0.5);
  modulator.offset(0.0);
#if MOD_FILTER_OUTPUT == MOD_BUTTERWORTH_FILTER_OUTPUT
  // Butterworth filter, 12 db/octave
  modchorus_filter_r.setLowpass(0, MOD_FILTER_CUTOFF_HZ, 0.707);
  modchorus_filter_l.setLowpass(0, MOD_FILTER_CUTOFF_HZ, 0.707);
#elif MOD_FILTER_OUTPUT == MOD_LINKWITZ_RILEY_FILTER_OUTPUT
  // Linkwitz-Riley filter, 48 dB/octave
  modchorus_filter_r.setLowpass(0, MOD_FILTER_CUTOFF_HZ, 0.54);
  modchorus_filter_r.setLowpass(1, MOD_FILTER_CUTOFF_HZ, 1.3);
  modchorus_filter_r.setLowpass(2, MOD_FILTER_CUTOFF_HZ, 0.54);
  modchorus_filter_r.setLowpass(3, MOD_FILTER_CUTOFF_HZ, 1.3);
  modchorus_filter_l.setLowpass(0, MOD_FILTER_CUTOFF_HZ, 0.54);
  modchorus_filter_l.setLowpass(1, MOD_FILTER_CUTOFF_HZ, 1.3);
  modchorus_filter_l.setLowpass(2, MOD_FILTER_CUTOFF_HZ, 0.54);
  modchorus_filter_l.setLowpass(3, MOD_FILTER_CUTOFF_HZ, 1.3);
#endif

  // internal mixing of original signal(0), reverb(1) and chorus(2)
  mixer_r.gain(VOL_MAIN, 0.5);
  mixer_l.gain(VOL_MAIN, 0.5);
  mixer_r.gain(VOL_REVERB, 0.2);
  mixer_l.gain(VOL_REVERB, 0.2);
  mixer_r.gain(VOL_CHORUS, 0.2);
  mixer_l.gain(VOL_CHORUS, 0.2);

  // Stereo/Mono initial setup
  if (configuration.mono == 0)
  {
    inverter.gain(-1.0); // change phase for second modulated delay (faked stereo mode)
  }
  else
  {
    inverter.gain(1.0);
    configuration.pan = ENC_MASTER_PAN_DEFAULT;
  }

  // set master volume
  set_master_volume(master_volume);

  // load last configuration used
  initial_values_from_eeprom();

  // init random generator
  srand(analogRead(A0));

  Serial.println(F("<setup end>"));

#if defined (SHOW_DEBUG) && defined (SHOW_CPU_LOAD_MSEC)
  Serial.println();
  show_cpu_and_mem_usage();
  cpu_mem_millis = 0;
#endif
}

//*************************************************************************************************
//* MAIN LOOP
//*************************************************************************************************

void loop()
{
  check_midi_devices();

  // CONTROL-RATE-EVENT-HANDLING
  if (control_rate > CONTROL_RATE_MS)
  {
    control_rate = 0;
    handle_ui();

    if ( eeprom_config_update_flag > 0 && ep.getActiveVoices() == 0) // write only to eeprom when no voice is active
      eeprom_config_update();

    if (eeprom_master_volume_update_flag == true && eeprom_master_volume_update_timer > STORE_MASTER_VOLUME_MS && ep.getActiveVoices() == 0)
      eeprom_master_volume_update();
  }

#if defined (SHOW_DEBUG) && defined (SHOW_CPU_LOAD_MSEC)
  if (cpu_mem_millis > SHOW_CPU_LOAD_MSEC)
  {
    show_cpu_and_mem_usage();
    cpu_mem_millis = 0;
  }
#endif

#ifdef DEBUG_AUDIO
  if (debug_audio_timer > DEBUG_AUDIO)
  {
    ep.noteOn(60 + rand() % 108, rand() % 128);
    debug_audio_timer = 0;
  }
#endif
}

//*************************************************************************************************
//* PROGRAM FUNCTIONS
//*************************************************************************************************
void handleNoteOn(byte inChannel, byte inNumber, byte inVelocity)
{
  if (checkMidiChannel(inChannel))
  {
    ep.noteOn(inNumber + configuration.transpose, inVelocity);
  }
}

void handleNoteOff(byte inChannel, byte inNumber, byte inVelocity)
{
  if (checkMidiChannel(inChannel))
  {
    ep.noteOn(inNumber + configuration.transpose, 0);
  }
}

void handleControlChange(byte inChannel, byte inData1, byte inData2)
{
  if (checkMidiChannel(inChannel))
  {
    switch (inData1)
    {
      // Standard MIDI-CC
      case MIDI_CC_PANORAMA:  // Panorama
        configuration.pan = map(inData2, 0, 127, ENC_MASTER_PAN_MIN, ENC_MASTER_PAN_MAX);
        break;
      case MIDI_CC_REVERB_SEND:  // Reverb level
        set_reverb_level(map(inData2, 0, 127, ENC_REVERB_LEVEL_MIN, ENC_REVERB_LEVEL_MAX));
        break;
      case MIDI_CC_TREMOLO_DEPTH:  // Tremolo level (same as modwheel)
        inData1 = 1; // now it's modwheel and can be processd by ep.processMidiController :-)
        break;
      case MIDI_CC_CHORUS_SEND:  // Chorus level
        set_chorus_level(map(inData2, 0, 127, ENC_CHORUS_LEVEL_MIN, ENC_CHORUS_LEVEL_MAX));
        break;
      case MIDI_CC_DETUNE_DEPTH:  // Detune level
        ep.setDetune(mapfloat(float(inData2), 0, 127, 0.0, 1.0));
        break;
      // Own MIDI-CC mapping
      case MIDI_CC_EP_DECAY:
        ep.setDecay(mapfloat(float(inData2), 0, 127, 0.0, 1.0));
        break;
      case MIDI_CC_EP_RELEASE:
        ep.setRelease(mapfloat(float(inData2), 0, 127, 0.0, 1.0));
        break;
      case MIDI_CC_EP_HARDNESS:
        ep.setHardness(mapfloat(float(inData2), 0, 127, 0.0, 1.0));
        break;
      case MIDI_CC_EP_TREBLE:
        ep.setTreble(mapfloat(float(inData2), 0, 127, 0.0, 1.0));
        break;
      case MIDI_CC_EP_STEREO:
        ep.setStereo(mapfloat(float(inData2), 0, 127, 0.0, 1.0));
        break;
      case MIDI_CC_EP_TUNE:
        ep.setTune(mapfloat(float(inData2), 0, 127, 0.0, 1.0));
        break;
      case MIDI_CC_EP_VELOCITY_SENSE:
        ep.setVelocitySense(mapfloat(float(inData2), 0, 127, 0.0, 1.0));
        break;
      case MIDI_CC_EP_TREM_FRQ:
        ep.setPanLFO(mapfloat(float(inData2), 0, 127, 0.0, 1.0));
        break;
      case MIDI_CC_EP_OVERDRIVE:
        ep.setOverdrive(mapfloat(float(inData2), 0, 127, 0.0, 1.0));
        break;
      case MIDI_CC_COMP_GAIN:
        set_comp_gain(map(inData2, 0, 127, ENC_COMP_GAIN_MIN, ENC_COMP_GAIN_MAX));
        break;
      case MIDI_CC_COMP_REPOSNE:
        set_comp_response(map(inData2, 0, 127, ENC_COMP_RESPONSE_MIN, ENC_COMP_RESPONSE_MAX));
        break;
      case MIDI_CC_COMP_LIMIT:
        set_comp_limit(map(inData2, 0, 127, ENC_COMP_LIMIT_MIN, ENC_COMP_LIMIT_MAX));
        break;
      case MIDI_CC_COMP_THRESHOLD:
        set_comp_threshold(map(inData2, 0, 127, ENC_COMP_THRESHOLD_MIN, ENC_COMP_THRESHOLD_MAX));
        break;
      case MIDI_CC_COMP_ATTACK:
        set_comp_attack(map(inData2, 0, 127, ENC_COMP_ATTACK_MIN, ENC_COMP_ATTACK_MAX));
        break;
      case MIDI_CC_COMP_DECAY:
        set_comp_decay(map(inData2, 0, 127, ENC_COMP_DECAY_MIN, ENC_COMP_DECAY_MAX));
        break;
      case MIDI_CC_REVERB_ROOMSIZE:
        set_reverb_roomsize(map(inData2, 0, 127, ENC_REVERB_ROOMSIZE_MIN, ENC_REVERB_ROOMSIZE_MAX));
        break;
      case MIDI_CC_REVERB_DAMPING:
        set_reverb_damping(map(inData2, 0, 127, ENC_REVERB_DAMPING_MIN, ENC_REVERB_DAMPING_MAX));
        break;
      case MIDI_CC_CHORUS_FREQUENCY:
        set_chorus_frequency(map(inData2, 0, 127, ENC_CHORUS_FREQUENCY_MIN, ENC_CHORUS_FREQUENCY_MAX));
        break;
      case MIDI_CC_CHORUS_INTENSITY:
        set_chorus_intensity(map(inData2, 0, 127, ENC_CHORUS_INTENSITY_MIN, ENC_CHORUS_INTENSITY_MAX));
        break;
      case MIDI_CC_CHORUS_WAVEFORM:
        set_chorus_waveform(map(inData2, 0, 127, ENC_CHORUS_WAVEFORM_MIN, ENC_CHORUS_WAVEFORM_MAX));
        break;
      case MIDI_CC_BASS_LR_LEVEL:
        set_bass_lr_level(map(inData2, 0, 127, ENC_BASS_LR_LEVEL_MIN, ENC_BASS_LR_LEVEL_MAX));
        break;
      case MIDI_CC_BASS_MONO_LEVEL:
        set_bass_mono_level(map(inData2, 0, 127, ENC_BASS_MONO_LEVEL_MIN, ENC_BASS_MONO_LEVEL_MAX));
        break;
      case MIDI_CC_EQ_BASS:
        set_eq_bass(map(inData2, 0, 127, ENC_EQ_BASS_MIN, ENC_EQ_BASS_MAX));
        break;
      case MIDI_CC_EQ_TREBLE:
        set_eq_treble(map(inData2, 0, 127, ENC_EQ_TREBLE_MIN, ENC_EQ_TREBLE_MAX));
        break;
      case MIDI_CC_MIDI_SOFT_THRU:
        set_midi_soft_thru(map(inData2, 0, 127, ENC_MIDI_SOFT_THRU_MIN, ENC_MIDI_SOFT_THRU_MAX));
        break;
      case MIDI_CC_MONO:
        set_mono(map(inData2, 0, 127, ENC_MONO_MIN, ENC_MONO_MAX));
        break;
      default:
        ep.processMidiController(inData1, inData2);
        break;
    }
  }
}

void handleAfterTouch(byte inChannel, byte inPressure)
{
  ;
}

void handlePitchBend(byte inChannel, int inPitch)
{
  ;
}

void handleProgramChange(byte inChannel, byte inProgram)
{
  if (checkMidiChannel(inChannel))
  {
    sound = inProgram;
    load_sound();
    if (menu_system.get_currentScreen() == &load_sound_screen)
      menu_system.update();
  }
}

void handleSystemExclusive(byte * data, uint len)
{
  ;
}

void handleSystemExclusiveChunk(const byte * data, uint16_t len, bool last)
{
  ;
}

void handleTimeCodeQuarterFrame(byte data)
{
  ;
}

void handleAfterTouchPoly(byte inChannel, byte inNumber, byte inVelocity)
{
  ;
}

void handleSongSelect(byte inSong)
{
  ;
}

void handleTuneRequest(void)
{
  ;
}

void handleClock(void)
{
  ;
}

void handleStart(void)
{
  ;
}

void handleContinue(void)
{
  ;
}

void handleStop(void)
{
  ;
}

void handleActiveSensing(void)
{
  ;
}

void handleSystemReset(void)
{
  ;
}

void handleRealTimeSystem(void)
{
  ;
}

bool checkMidiChannel(byte inChannel)
{
  // check for MIDI channel
  if (configuration.midi_channel == MIDI_CHANNEL_OMNI)
  {
    return (true);
  }
  else if (inChannel != configuration.midi_channel)
  {
#ifdef SHOW_DEBUG
    Serial.print(F("Ignoring MIDI data on channel "));
    Serial.print(inChannel);
    Serial.print(F("(listening on "));
    Serial.print(configuration.midi_channel);
    Serial.println(F(")"));
#endif
    return (false);
  }
  return (true);
}

void set_master_volume(uint8_t value)
{
  //configuration.pan = 0; // BAD HACK!
  uint16_t tmp = map(value, ENC_MASTER_VOLUME_MIN, ENC_MASTER_VOLUME_MAX, 0, 0x3ff);
  float tmp2 = mapfloat(configuration.pan, ENC_MASTER_PAN_MIN, ENC_MASTER_PAN_MAX, 0.0, 1.0);
  float tmp3 = (float)(tmp * (tmp + 2)) / (float)(1 << 20);
#ifdef SHOW_DEBUG
  Serial.print(F("Setting volume: VOL="));
  Serial.print(value, DEC);
  Serial.print(F("["));
  Serial.print(tmp3, 3);
  Serial.print(F("] PAN="));
  Serial.print(configuration.pan, DEC);
  Serial.print(F("["));
  Serial.print(tmp2, 3);
  Serial.print(F("] "));
  Serial.print(tmp3 * sinf(tmp2 * PI / 2), 3);
  Serial.print(F("/"));
  Serial.println(tmp3 * cosf(tmp2 * PI / 2), 3);
#endif

  // float v = (float)(a * (a + 2))/(float)(1 << 20); //  (pseudo-) logarithmic curve for volume control
  // http://files.csound-tutorial.net/floss_manual/Release03/Cs_FM_03_ScrapBook/b-panning-and-spatialization.html
  volume_r.gain(tmp3 * sinf(tmp2 * PI / 2));
  volume_l.gain(tmp3 * cosf(tmp2 * PI / 2));
  if (configuration.mono == 2)
    volume_l.gain(0.0);
  else if (configuration.mono == 3)
    volume_r.gain(0.0);

  eeprom_master_volume_update_flag = true;
  eeprom_master_volume_update_timer = 0;

  if (menu_system.get_currentScreen() == &master_volume_screen)
    menu_system.update();
}

/******************************************************************************
   EEPROM HELPER
 ******************************************************************************/

void config_from_eeprom(void)
{
  uint32_t checksum;
  config_t tmp_conf;

  EEPROM_readAnything(EEPROM_CONFIGURATIONS + sizeof(config_t) * (sound - 1), tmp_conf);
  checksum = crc32((byte*)&tmp_conf + 4, sizeof(tmp_conf) - 4);

#ifdef SHOW_DEBUG
  Serial.print(F("Reading sound "));
  Serial.print(sound, DEC);
  Serial.print(F(" from 0x"));
  Serial.print(EEPROM_CONFIGURATIONS + sizeof(config_t) * (sound - 1), HEX);
  Serial.print(F(" EEPROM checksum: 0x"));
  Serial.print(tmp_conf.checksum, HEX);
  Serial.print(F(" / 0x"));
  Serial.print(checksum, HEX);
#endif

  if (checksum == tmp_conf.checksum)
  {
    EEPROM_readAnything(EEPROM_CONFIGURATIONS + sizeof(config_t) * (sound - 1), configuration);
#ifdef SHOW_DEBUG
    Serial.println(F(" - OK"));
#endif
  }
  else
  {
#ifdef SHOW_DEBUG
    Serial.println(F(" - mismatch -> loading initial configuration."));
#endif
    EEPROM.update(EEPROM_SOUND, sound);
  }
  set_complete_configuration();

#ifdef SHOW_DEBUG
  show_sound();
#endif
}

void initial_values_from_eeprom(void)
{
  master_volume = EEPROM.read(EEPROM_MASTER_VOLUME);
  sound = EEPROM.read(EEPROM_SOUND);
  load_sound();
}

void eeprom_config_write(uint8_t value)
{
  eeprom_config_update_flag = value;
}

void eeprom_config_update(void)
{
  configuration.checksum = crc32((byte*)&configuration + 4, sizeof(configuration) - 4);
  Serial.print(F("Updating EEPROM configuration for sound "));
  Serial.print(eeprom_config_update_flag, DEC);
  Serial.print(F(" with checksum 0x"));
  Serial.print(configuration.checksum, HEX);
  Serial.print(F(" at 0x"));
  Serial.println(EEPROM_CONFIGURATIONS + sizeof(config_t) * (eeprom_config_update_flag - 1), HEX);
  EEPROM_writeAnything(EEPROM_CONFIGURATIONS + sizeof(config_t) * (eeprom_config_update_flag - 1), configuration);
  eeprom_config_update_flag = 0;
  EEPROM.update(EEPROM_SOUND, sound);
}

void eeprom_master_volume_write(void)
{
  eeprom_master_volume_update_flag = true;
}

void eeprom_master_volume_update(void)
{
  eeprom_master_volume_update_flag = false;
  EEPROM.update(EEPROM_MASTER_VOLUME, master_volume);
  Serial.println(F("Updating EEPROM with master_volume"));
}

uint32_t crc32(byte * calc_start, uint16_t calc_bytes) // base code from https://www.arduino.cc/en/Tutorial/EEPROMCrc
{
  const uint32_t crc_table[16] =
  {
    0x00000000, 0x1db71064, 0x3b6e20c8, 0x26d930ac,
    0x76dc4190, 0x6b6b51f4, 0x4db26158, 0x5005713c,
    0xedb88320, 0xf00f9344, 0xd6d6a3e8, 0xcb61b38c,
    0x9b64c2b0, 0x86d3d2d4, 0xa00ae278, 0xbdbdf21c
  };
  uint32_t crc = ~0L;

  for (byte* index = calc_start ; index < (calc_start + calc_bytes) ; ++index)
  {
    crc = crc_table[(crc ^ *index) & 0x0f] ^ (crc >> 4);
    crc = crc_table[(crc ^ (*index >> 4)) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }

  return (crc);
}

//*************************************************************************************************
//* DEBUG FUNCTIONS
//*************************************************************************************************

#if defined (SHOW_DEBUG) && defined (SHOW_CPU_LOAD_MSEC)
void show_cpu_and_mem_usage(void)
{
  Serial.print(F("CPU: "));
  Serial.print(AudioProcessorUsage(), DEC);
  Serial.print(F("   CPU MAX: "));
  Serial.print(AudioProcessorUsageMax(), DEC);
  Serial.print(F("  MEM: "));
  Serial.print(AudioMemoryUsage(), DEC);
  Serial.print(F("   MEM MAX: "));
  Serial.print(AudioMemoryUsageMax(), DEC);
  Serial.print(F("   RENDER_TIME_MAX: "));
  Serial.print(render_time_max, DEC);
  Serial.print(F("   XRUN: "));
  Serial.print(xrun, DEC);
  Serial.print(F("   OVERLOAD: "));
  Serial.print(overload, DEC);
  Serial.print(F("   PEAK: "));
  Serial.print(peak, DEC);
  Serial.print(F("   ACTIVE_VOICES: "));
  Serial.print(ep.getActiveVoices(), DEC);
  Serial.println();
  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();
  render_time_max = 0;
}

void show_sound(void)
{
  Serial.println(F("======SHOW=SOUND=CONFIGURATION======"));
  Serial.print(F("Master Volume: "));
  Serial.println(master_volume, DEC);
  Serial.print(F("Sound: "));
  Serial.println(sound, DEC);
  Serial.print(F("Checksum: 0x"));
  Serial.println(configuration.checksum, HEX);
  Serial.print(F("Decay: "));
  Serial.println(configuration.decay, DEC);
  Serial.print(F("Release: "));
  Serial.println(configuration.release, DEC);
  Serial.print(F("Hardness: "));
  Serial.println(configuration.hardness, DEC);
  Serial.print(F("Treble: "));
  Serial.println(configuration.treble, DEC);
  Serial.print(F("Stereo: "));
  Serial.println(configuration.stereo, DEC);
  Serial.print(F("Transpose: "));
  Serial.println(configuration.transpose, DEC);
  Serial.print(F("Tune: "));
  Serial.println(configuration.tune, DEC);
  Serial.print(F("Detune: "));
  Serial.println(configuration.detune, DEC);
  Serial.print(F("Velocity Sense: "));
  Serial.println(configuration.velocity_sense, DEC);
  Serial.print(F("Pan Tremolo Frequency: "));
  Serial.println(configuration.pan_trem_frequency, DEC);
  Serial.print(F("Pan Tremolo Level: "));
  Serial.println(configuration.pan_trem_level, DEC);
  Serial.print(F("Ovedrive: "));
  Serial.println(configuration.overdrive, DEC);
  Serial.print(F("Compressor Gain: "));
  Serial.println(configuration.comp_gain, DEC);
  Serial.print(F("Compressor Respone: "));
  Serial.println(configuration.comp_response, DEC);
  Serial.print(F("Compressor Limit: "));
  Serial.println(configuration.comp_limit, DEC);
  Serial.print(F("Compressor Threshold: "));
  Serial.println(configuration.comp_threshold, DEC);
  Serial.print(F("Compressor Attack: "));
  Serial.println(configuration.comp_attack, DEC);
  Serial.print(F("Compressor Decay: "));
  Serial.println(configuration.comp_decay, DEC);
  Serial.print(F("Reverb Roomsize: "));
  Serial.println(configuration.reverb_roomsize, DEC);
  Serial.print(F("Reverb Damping: "));
  Serial.println(configuration.reverb_damping, DEC);
  Serial.print(F("Reverb Level: "));
  Serial.println(configuration.reverb_level, DEC);
  Serial.print(F("Chorus Frequency: "));
  Serial.println(configuration.chorus_frequency, DEC);
  Serial.print(F("Chorus Intensity: "));
  Serial.println(configuration.chorus_intensity, DEC);
  Serial.print(F("Chorus Waveform: "));
  Serial.println(configuration.chorus_waveform, DEC);
  Serial.print(F("Chorus Level: "));
  Serial.println(configuration.chorus_level, DEC);
  Serial.print(F("Bass L/R Level: "));
  Serial.println(configuration.bass_lr_level, DEC);
  Serial.print(F("Bass Mono Level: "));
  Serial.println(configuration.bass_mono_level, DEC);
  Serial.print(F("EQ Bass: "));
  Serial.println(configuration.eq_bass, DEC);
  Serial.print(F("EQ Treble: "));
  Serial.println(configuration.eq_treble, DEC);
  Serial.print(F("Loudness: "));
  Serial.println(configuration.loudness, DEC);
  Serial.print(F("MIDI Channel: "));
  Serial.println(configuration.midi_channel, DEC);
  Serial.print(F("MIDI Soft-Thru: "));
  Serial.println(configuration.midi_soft_thru, DEC);
  Serial.print(F("Maximum Polyphony: "));
  Serial.println(configuration.max_poly, DEC);
  Serial.print(F("Audio-Mono: "));
  Serial.println(configuration.mono, DEC);
  Serial.print(F("Panorama: "));
  Serial.println(configuration.pan, DEC);
  Serial.println(F("======END=OF=SOUND=CONFIGURATION======="));
}
#endif
