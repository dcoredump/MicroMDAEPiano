/* Audio Library for Teensy 3.X
   Copyright (c) 2014, Pete (El Supremo)
   Copyright (c) 2019-2020, Holger Wirtz

   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:

   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.

   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
*/

#include <Arduino.h>
#include <Audio.h>
#include "arm_math.h"
#include "effect_modulated_delay.h"
#include "config.h"

extern config_t configuration;

/******************************************************************/

// Based on;      A u d i o E f f e c t D e l a y
// Written by Pete (El Supremo) Jan 2014
// 140529 - change to handle mono stream - change modify() to voices()
// 140219 - correct storage class (not static)
// 190527 - added modulation input (by Holger Wirtz)

boolean AudioEffectModulatedDelay::begin(short *delayline, uint16_t d_length)
{
#if 0
  Serial.print(F("AudioEffectModulatedDelay.begin(modulated-delay line length = "));
  Serial.print(d_length);
  Serial.println(F(")"));
#endif

  _delayline = NULL;
  _delay_length = 0;
  _cb_index = 0;
  _delay_offset = 0;

  if (delayline == NULL)
    return (false);
  if (d_length < 10)
    return (false);

  _delayline = delayline;
  _delay_length = d_length;
  memset(_delayline, 0, _delay_length * sizeof(int16_t));
  _delay_offset = _delay_length >> 1 ;

  return (true);
}

uint16_t AudioEffectModulatedDelay::get_delay_length(void)
{
  return (_delay_length);
}

void AudioEffectModulatedDelay::update(void)
{
  audio_block_t *block;
  audio_block_t *modulation;

  if (_delayline == NULL)
    return;

  block = receiveWritable(0);
  modulation = receiveReadOnly(1);

  if (block && modulation)
  {
    int16_t *bp;
    int16_t cb_mod_index_neighbor;
    float *mp;
    float mod_index;
    float mod_number;
    float mod_fraction;
    float modulation_f32[AUDIO_BLOCK_SAMPLES];

    bp = block->data;
    arm_q15_to_float(modulation->data, modulation_f32, AUDIO_BLOCK_SAMPLES);
    mp = modulation_f32;

    for (uint16_t i = 0; i < AUDIO_BLOCK_SAMPLES; i++)
    {
      // write data into circular buffer (delayline)
      if (_cb_index >= _delay_length)
        _cb_index = 0;
      _delayline[_cb_index] = *bp;

      // calculate the modulation-index as a floating point number for interpolation
      mod_index = *mp * _delay_offset;
      mod_fraction = modff(mod_index, &mod_number); // split float of mod_index into integer (= mod_number) and fraction part

      // calculate modulation index into circular buffer
      cb_mod_index = _cb_index - (_delay_offset + mod_number);
      if (cb_mod_index < 0) // check for negative offsets and correct them
        cb_mod_index += _delay_length;

      if (cb_mod_index == _delay_length - 1)
        cb_mod_index_neighbor = 0;
      else
        cb_mod_index_neighbor = cb_mod_index + 1;

      *bp = round(float(_delayline[cb_mod_index]) * mod_fraction + float(_delayline[cb_mod_index_neighbor]) * (1.0 - mod_fraction));

      // push the pointers forward
      bp++; // next audio data
      mp++; // next modulation data
      _cb_index++; // next circular buffer index
    }
  }

  if (modulation)
    release(modulation);

  if (block)
  {
    transmit(block, 0);
    release(block);
  }
}
